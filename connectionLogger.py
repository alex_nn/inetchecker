import socket
import time

REMOTE_SERVER1 = "www.google.com"
REMOTE_SERVER2 = "www.facebook.com"
REMOTE_SERVER3 = "www.ea.com"

TIMEOUT = 5

def is_connected(serverUrl):
  try:
    # see if we can resolve the host name -- tells us if there is
    # a DNS listening
    host = socket.gethostbyname(serverUrl)
    # connect to the host -- tells us if the host is actually
    # reachable
    s = socket.create_connection((host, 80), 2)
    return True
  except:
     pass
  return False

def check_connections():
    start = time.time()
    connectionState =  is_connected(REMOTE_SERVER1)
    end = time.time() - start
    if (connectionState):
        print time.ctime() + REMOTE_SERVER1 + " response time: " + str(end)
        return True
    else:
        start = time.time()
        connectionState =  is_connected(REMOTE_SERVER2)
        end = time.time() - start
        if (connectionState):
            print time.ctime() + REMOTE_SERVER2 + " response time: " + str(end)
            return True
        else:
            start = time.time()
            connectionState =  is_connected(REMOTE_SERVER3)
            end = time.time() - start
            if (connectionState):
                print time.ctime() + REMOTE_SERVER3 + " response time: " + str(end)
                return True
            else:
                return False


def main():
    logFile = open('./logfile', 'w+', 0)
    prevVal = check_connections()
    prevTime = time.ctime()
    needLog = False
    if (prevVal == False):
        needLog = True
    logFile.write("Logging started: " + prevTime + "\n")
    while(True):
        currentVal = check_connections()
        if (currentVal != prevVal):
            prevVal = currentVal
            prevTime = time.ctime()
            needLog = True
        logStr = "";
        if (currentVal == False):
            logStr = time.ctime() + ": No internet connection since: " + prevTime
        if (currentVal == True):
            logStr =  time.ctime() + ": Connected to internet since: " + prevTime
        print logStr
        if (needLog):
            logFile.write(logStr)
            logFile.write("\n")
            needLog = False
        time.sleep(TIMEOUT)

main()
